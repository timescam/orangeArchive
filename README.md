# Orange Archive

> no longer needed after Blue Archive splits versions between standard and teen, as standard is no longer censored 

Uncensor Blue Archive global with one click (Android only)

Tested on both `ASUS_I001DC (Android 10)` and `BlueStack (5.4.0.1063 N64)`

For `Anroid 11` or above [`ADB WIFI`](A11/README.md) is needed

[中文 support thread](https://bbs.nga.cn/read.php?tid=29897667)

## Disclaimer

>I'm no legal expert, but I'm pretty sure modifications of game files is against Blue Archive's TOS
>
>So do this at your own risk, I'm and will not be responsible for any bans/loss
>
>This application currently contains zero asset from the game, and all files copied/moved are from the game itself.

## Getting Started

### Important

>If you already patch the game with the previous release, restore first, then install the new release to patch it again.

### Steps

1. Get the latest version [here](https://gitlab.com/timescam/orangeArchive/-/raw/v0.3/Orange_Archive.apk?inline=false)

2. Install the apk (Google Play protect might flag it, ignore it)

3. Go to your phone's setting, and grant the app `Storage Access` for it to access BA's game files. (Starting from `Android 10`, it may also need `Draw Over Other Apps` permission)

4. For `Anroid 11` or above [`ADB WIFI`](A11/README.md) is also needed

5. Run the app to apply the patch. (It may as for root to kill app, not necessary to do so)

6. Run it again to restore.

## I don't trust your app

If you have [Tasker](https://tasker.joaoapps.com/) installed, import the [`tsk.xml`](Orange_Archive.tsk.xml) from this repo, look at it and run/package it yourself.

[What files are being modified(rename) currently](DIY.md)

## Credits

This app is created with [Tasker](https://tasker.joaoapps.com/) and [Tasker App Factory](https://tasker.joaoapps.com/userguide/en/appcreation.html).

[亂雲彼端 on NGA](https://bbs.nga.cn/read.php?pid=573582670&opt=128) for Alice Intro CG.

[hdk5V on twitter](https://twitter.com/hdk5V/status/1472535755478159366?s=20) for another Alice Intro CG.

All product names, logos, and brands are property of their respective owners.

## Future Plans

I'll update it when there are new asset(s) being censored and with the original asset(s) still packaged in the global release.

I won't be always monitoring the game, so if there's anything I missed, create an issue with the provided template. And feel free to do a Pull Request, if you have the technical knowhows.

## TODO

Complete the documentation for files that have been modify, for better documentation of the patch and for people who want to manually replace the files.

## Why the naming

~Opposite color of blue is orange, I'm bad at naming projects~

## License

[WTFPL](LICENSE)

## Similar projects

[CunnyArchive](https://github.com/hdk5/CunnyArchive) (Works on all Android versions, including 11+, without any special trickery on the user side)
