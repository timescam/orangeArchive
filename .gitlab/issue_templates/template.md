```plain text
Before creating this issue please double check if there is an existing issue with the same situation

If is ban related report it in [Ban Report Megathread](https://gitlab.com/timescam/orangeArchive/-/issues/1) instead

Remove me if you understand
```

## Summary

(Summarize the censored scene encountered concisely)

## What is the expected correct behavior?

(link to og image is preferable)

## Relevant logs and/or screenshots

(Other screenshots relating to this issue)

## Possible fixes

(If you can, find the modify and original file path. Or better do a [Pull Request](https://gitlab.com/timescam/orangeArchive/-/merge_requests/new))
