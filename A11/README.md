# Android 11 or newer versions

## ADB WIFI permission required

guide: [https://tasker.joaoapps.com/userguide/en/help/ah_adb_wifi.html](https://tasker.joaoapps.com/userguide/en/help/ah_adb_wifi.html)

## Why is it needed

[docs](https://developer.android.com/about/versions/11/privacy/storage#other-apps-data)

tldr: In android 11 apps now cant access other apps' resources, using adb wifi allows bypassing that restriction
